package main

import (
	"fmt"
	"github.com/rs/cors"
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

func main() {

	http.HandleFunc("/signup", signup)
	http.HandleFunc("/gocli", gocli)
	http.HandleFunc("/login", loginPage)
	http.HandleFunc("/gohelpfuls", gohelpfuls)
	http.HandleFunc("/tempdata", tempdata)
	http.HandleFunc("/linux", linux)
	http.HandleFunc("/resources", resources)
	http.HandleFunc("/links", links)
	http.HandleFunc("/get", get)
	http.HandleFunc("/show", show)
	http.HandleFunc("/", index)
	http.HandleFunc("/users/goofy", set)
	http.HandleFunc("/users/goofy/read", read)

	handler := http.HandlerFunc(links)
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})

	handler = http.HandlerFunc(show)
	c = cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
	log.Fatal(http.ListenAndServe(":8180", c.Handler(handler)))
}

func index(w http.ResponseWriter, req *http.Request) {
	
}
