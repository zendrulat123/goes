package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"io/ioutil"
	"log"
	"net/http"
	_ "src/github.com/go-sql-driver/mysql"
	"strings"
	"time"
)

var err error

type User struct {
	Id     string `json:"id,omitempty"`
	Userid string `json:"userid,omitempty"`
	Link   string `json:"link,omitempty"`
	Meta   string `json:"meta,omitempty"`
}

var user []User

func debug(data []byte, err error) {
	if err == nil {
		fmt.Printf("%s\n\n", data)
	} else {
		log.Fatalf("%s\n\n", err)
	}
}

//get handler
func show(w http.ResponseWriter, r *http.Request) {

	//checking header
	fmt.Println(w.Header())
	fmt.Println("started")
	fmt.Println("Post is chosen")
	fmt.Println(r.Header.Get("Origin"))
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	var meta string
	var link string
	var userid string
	var id string
	rows, err := db.Query("SELECT * FROM testdb.links")
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		err = rows.Scan(&id, &userid, &link, &meta)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("this is the link ", link)
		fmt.Println("this is the meta ", meta)
		fmt.Println("this is the id ", id)
		fmt.Printf("[%q]", strings.Trim(link, "www."))
		link = strings.Trim(link, "www.")

		user = append(user, User{Id: id, Userid: userid, Link: link, Meta: meta})
	}

	rows.Close()
	spew.Dump(user)

	json.NewEncoder(w).Encode(user)
	user = nil
	fmt.Println("worked")
	set(w, r)
}

//get handler
func get(w http.ResponseWriter, r *http.Request) {

	//checking header
	fmt.Println(w.Header())
	fmt.Println("started")
	fmt.Println("Post is chosen")
	fmt.Println(r.Header.Get("Origin"))
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")

	/*get the data from the form
	 ************************************/

	r.ParseForm()

	queryMap := r.Form
	fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", r.PostForm)
	metas := r.FormValue("meta")
	links := r.FormValue("links")
	fmt.Fprintf(w, "metas = %s\n", metas)
	fmt.Fprintf(w, "links = %s\n", links)
	spew.Dump(metas, links)

	/*starting the request
	 ************************************/
	switch r.Method {
	case http.MethodGet:

		// Handle GET requests
		w.WriteHeader(http.StatusOK)
		return
	case http.MethodPost:
		// Handle POST requests
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		fmt.Println("reqeusted boyd ", r.Body)
		fmt.Println("body ", body)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf("Query string values: %s\nBody posted: %s", queryMap, body)))
		fmt.Println("querymap is ", queryMap)
		/*starting the db
		 ************************************/
	
		if err != nil {
			log.Fatal(err)
		}
		defer db.Close()
		//test the db
		err = db.Ping()
		if err != nil {
			log.Fatal(err)
		}
		//get the data
		Meta := strings.Join(queryMap["meta"], ", ")
		Link := strings.Join(queryMap["link"], ", ")

		res, err := db.Exec(

			"INSERT INTO testdb.links(link, meta) VALUES('" + Link + "', '" + Meta + "')")
		//"INSERT INTO testdb.links(link, meta) VALUES('www.bing.com', 'search')")
		if err != nil {
			log.Fatal(err)
		}
		rowCount, err := res.RowsAffected()
		if err != nil {
			log.Fatal(err)
		}

		spew.Dump(Link)
		spew.Dump(Meta)
		spew.Dump(rowCount)

		// rowCount = nil
		Link = ""
		Meta = ""
		queryMap = nil
		return
	}
	http.Redirect(w, r, "/links", 301)
	w.WriteHeader(http.StatusMethodNotAllowed)

} //end of get


func links(res http.ResponseWriter, req *http.Request) {

	// process form submission
	switch req.Method {
	case "GET":
		err := tpl.ExecuteTemplate(res, "links.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		fmt.Println(req.Header.Get("Origin"))
		allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
		res.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		res.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
		res.Header().Set("Access-Control-Expose-Headers", "Authorization")
		err := tpl.ExecuteTemplate(res, "links.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	default:
		fmt.Fprintf(res, "Sorry, only GET and POST methods are supported.")
	}

}

func golang(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "golang.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func signup(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "signup.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func tempdata(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "tempdata.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func gocli(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "gocli.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func gohelpfuls(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "gohelpfuls.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func linux(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "linux.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func resources(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "resources.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
func loginPage(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "login.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}

func HandleError(w http.ResponseWriter, err error) {
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatalln(err)
	}
}
